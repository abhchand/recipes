Peanut Butter energy balls

Oats ( any kind rolled or quick). 2 cups
Peanut butter/ almond butter 2 cups
Walnut  powder 1/4 cup
Slices almond 1/4 cup
Chocolate chips ( optional) 1/4 cup
Flax seed powder (optional) 1/8 cup
Honey 1/4 cup ( increase according to the ingredients )

Method:
Add all ingredients well and make onto a tight ball. Roll the ball in chocolate or coconut or almond powder as per your taste . Enjoy 🤗

Note all Ingredients selection and amount to suit one’s taste. The measurements given is a thumb rule 😀 

Source: Amma

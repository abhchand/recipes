Spring Rolls

20           Spring Roll Wraps
1 lb         Tofu

2 1/2 cups   Cabbage, shredded
2 1/2 cups   Carrots, shredded
1 cup        Green Onions, chopped
1 cup        Purple Onions, chopped

1 tsp        Ginger, minced
1 tbsp       Garlic, minced

1/4 cup      Soy sauce
2 tbsp       Seasame Oil

2 tbsp       Sesame seeds, toasted



1. Heat sesame oil in large skillet

2. Add ginger, green onions, onions, and garlic. Cook on high heat 2-3 mins, stirring often

3. Add remaining ingredients (cabbage, tofu, carrots, soy sauce, and sesame seeds) and cook for 4-6 minutes until vegetables are tender. Let mixture cool.

4. Roll filling into wraps. Use water to seal flap.

5. Bake at 400F for 20-25 mins, inverting rolls halfway through.


Other options:

  - air fry at 400F for 10 mins.
  - fry in oil at 350F. Place roll's flap down, a few at a time, turning ocassionally until golden, 2-3 mins

USAL
A healthy meal of spicy pulses

Serves two

( All measurements are approximate. You can use your best judgement)


Ingredients for the gravy:
2 large Onions (cut into big cubes)
1/2 tbsp turmeric powder
1/2 tbsp red chilli powder
1tbsp coriander power
1 tbsp jeera powder
1/2 tbsp ginger paste
1 tbsp garlic paste
salt to taste

For the basic gravy:
1) Heat 3 tbsp oil in a pan.
2) Add the onion cubes and saute few seconds.
3) now add all the above mentioned spices including salt to the onions and saute it till onions start turning translucent. Stir the onions to let the spices mix well and cover the pan with a lid so that it can cook faster. You will easily know when the onions look cooked.
4) Once the onions are cooked, turn of the gas and let it cool down.
5) Once cooled, grind the onions in magic bullet/ any mixer.
6) Your gravy is ready.

This gravy is the basic for many Indian dishes, hence you can use this for any subzi. You can also store it in the freezer for future use..

Ingredients for Usal

1/2 cup white pea
1/2 cup black chana
1/2 cup green moong
1/2 tbsp mustard seeds
3tbsp grated coconut
a little tamarind concentrate (depends on the kind of conc.)
1 tsp garam masala

1) Soak the white pea, black chana and green moong in water overnight. The next day wash it, fill in fresh water, add little salt and pressure cook them. If it gets overcooked, it doesn't matter.

Procedure for the Usal:

1) Heat a teaspoon of oil in a pan. Add the mustard seeds and let it splutter. Now add the onion gravy prepared earlier and stir it.
2) Now add all the cooked pulses to this gravy and mix well. If the entire mixture is too thick, you can add some water to loosen the consistency.
3) Now add the garam masala, grated coconut and the tamarind water to this mixture and mix it well. At this stage, you add other spices as per your taste. You can add salt, chilli, etc if you feel the need to.

Be careful with tamarind water as the concentrate can be very sour. So it is good to add little and then add more if needed.

Serving suggestions:

In a bowl, place a slice of bread. Add usal on top of the bread. Top it with some beaten curd (sour cream is also a good substitute) and date chutney. Then finish it with some chopped onions and chopped coriander leaves.



P.S. If you do not want to go through the hassle of making the gravy, then there is a readymade option. At the Indian store, you get " Patak's tikka paste ". ' Patak '  is the name of the brand. It comes in a glass bottle and looks like pickle. But it is not spicy. It has everything that a basic gravy has and tastes very good. When I fall short of mine, I use it as a substitute.

You can use like 6 tbsp of the gravy for the paste for this recipe.

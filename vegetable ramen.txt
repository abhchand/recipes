Vegetable Ramen

Serving Size: XX
Preparation Time


INGREDIENTS

    4 packets ramen
    8 oz mushrooms
    1 Red Bell Pepper
    1 White Onion
    X cups carrots (3 carrots - how many cups is that?)
    1 cup Peas
    1 cup Corn
    2 packets Ramen powder

INSTRUCTIONS

    1. Boil X cups of water

    2. Boil / Cook peas, carrots, and corn ahead of time. Microwave 75 secs each.

    3. Add all packets of Ramen, let it cook till soft

    4. Cook carrots, bell peppers, mushrooms, peas, corn, and onions last in separate bowl.
       Cook minimally until onions still cripsy. Should not be soggy.

    5. Add in vegetables. Boil for 5 mins

    6. Stir in Ramemn powder. Mix well. Let cool for 1-2 mins.

    7. Drain water as needed.

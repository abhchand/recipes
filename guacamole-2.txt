Guacamole

	* 3 Avacados
	* 1/4 Onion (small, minced)
	* 2 cloves of garlic (minced)
	* 2 Jalapeno Peppers (minced)
	* Salt
	* Pepper
	* Lime Juice
	* Cilantro (Roughly 3 Tablespoons cut up)

Mix and mash all ingredients with just enough salt, pepper, and lime juice to taste.


Original Source: Vanessa Yip

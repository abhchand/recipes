=== Stir Fry

1 tablespoon olive oil
* 1 white onion, thinly sliced
* 1 red bell pepper, thinly sliced
* 1 cup carrots sliced
* 1 cup mushrooms sliced
* 2 cups (heads) broccoli
* 1 cup baby corn
* 1 zucchinni, cut in semicircle

* 1/4 cup soy sauce
* 2 garlic cloves minced
* 3 Tablespoons brown sugar
* 1 teaspoon sesame oil
* 1/2 cup vegetable broth
* 1 tablespoon cornstarch (or 2 tbsp wheat flour)

* sesame seeds for garnish



1. Create sauce - In a small bowl, whisk together soy sauce, garlic, brown sugar, sesame oil,  broth, and cornstarch.

2. In a wok or large skillet add 1 Tablespoon olive oil over medium high heat. Add all vegetables. Sauté 2-3 minutes until veggies are almost tender.

3. Mix in sauce and let vegetables cook for ~5 mins.

Source: https://therecipecritic.com/vegetable-stir-fry/

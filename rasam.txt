3/4 Cup Toor Dal (no stones)


1. Wash toor dal (no stones)
2. Put it in pressure cooker. Soak in water for 5 mins. Mix it. Add fresh water (3/4 full)
3. Get it to pressure, then cook for 3 - 5 mins
4. Cut tomato into small pieces (and gar)
5. 2 tsps of oil in a pan. Put mustard seeds, cumin seeds, hing, curry leaves.
6. Small amount of ground up black pepper (1/4 tsp?). . Once it starts to crackle, put tomato (and ga). Sautee
7. Once it gets mushy, pressure comes down, add the Daal. Make it extra thicc
8. Add salt to taste
9. Add Rasam powder (1/2 tsp). 1/8tsp tamarind.
10 Mix everything. Add cilantro optionally.


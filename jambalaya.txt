Source: https://www.lifeasastrawberry.com/spicy-vegan-jambalaya/
Ingredients

  - 3 Tbsp. extra virgin olive oil
  - 1 large yellow onion, diced
  - 3 cloves garlic, chopped
  - 4 large stalks celery, diced
  - 1 heaping Tablespoon diced jalapeño (use more or less depending on how spicy you like things – 1 Tbsp. gives it a nice kick)
  - 4 cups diced fresh tomatoes (you could also use whole cherry tomatoes or one large can of crushed tomatoes)
  - 2 cups uncooked brown rice
  - 4.5 cups vegetable stock
  - 2 teaspoons vegan worcestershire sauce (you can find some good vegan varieties online or, if you don’t have any of the vegan stuff handy, just leave it out!)
  - 3 bay leaves
  - 1 teaspoon smoked paprika
  - 2 teaspoons hot sauce (I use Sriracha)
  - salt and pepper to taste
  - 1.5 cups chopped cilantro, plus extra for garnish

Instructions

  1. Heat oil in a large skillet or saucepan (use one that has a tight-fitting lid).
  2. Add onion, garlic, celery, and jalapeño to oil and sauté until onions are translucent, about 3 minutes.
  3. Add tomatoes and cook an additional minute or two to soften them up.
  4. Add rice, vegetable stock, worcestershire sauce, bay leaves, paprika, hot sauce, salt, and pepper to pan and stir to combine. Cover and bring mixture to a simmer. Let it cook for 20-40 minutes, until the rice has absorbed all the liquid and cooked through.
  5. Stir in fresh cilantro and serve immediately. Garnish with extra cilantro.

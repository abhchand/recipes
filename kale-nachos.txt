=== Ingredients

1/2 cup of Cilantro
1 clove of garlic
3 tbps cream cheese
2 tbps tomatillo salsa (green)
1 tsp lime juice

1/3 can refried beans
1/3 chopped onions
1.5 cups kale

1 bag of nachos (blue or red colored)

=== Instructions

1. Add olive oil, salt, pepper to Kale. Mix throughly. Bake kale in oven at 300 deg for 20 mins

2. To make crema, blend cream cheese, tomatillo salsa, lime juice, garlic.

3. Warm up refried beans.

4. To assemble, spread nachos on plate. Spread refried beans and add onions on top. Then add kale on top and drizzle with crema.

